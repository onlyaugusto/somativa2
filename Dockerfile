FROM ubuntu:latest

RUN apt-get update
RUN apt-get install nginx -y

COPY index.html /var/www/html/
COPY styles.css /var/www/html/

RUN chown -R www-data:www-data /var/www/html && \
    chmod -R 755 /var/www/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]